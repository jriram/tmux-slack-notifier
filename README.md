# How to install

1. `$ mkdir -p ~/bin`
1. `$cd ~/bin`
1. `$ wget tmux-slack-notifier.sh (use the raw view url)`
1. `$ chmod +x tmux-slack-notifier.sh`
1. Get your Slack token
1. Write your Slack token in the script
1. `$ wget https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64`
1. `$ mv jq-linux64 jq`
1. `$ chmod +x jq`
1. `$ mkdir -p ~/.tmux-slack-notifier/`
1. Add the lines in `tmux.conf` to your tmux configuration file